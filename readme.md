### Docker image

Installed compilers:

- Java openjdk 11.0.10
- C++ gcc version 9.3.0
- Python2 2.7.18
- Python3 3.8.5
- NodeJs v14.16.1, npm 6.14.12
- PascalABC.NET 3.6.2
- Free Pascal 3.0.4
- PHP 7.4.3
- Golang 1.13.8
- Kotlin 1.3.72
- Swift  5.3.3
- Ruby 2.7.0

Installed 3rd programs:

- [time(1)](http://man7.org/linux/man-pages/man1/time.1.html) for memory utilization info, can be used other see refs
